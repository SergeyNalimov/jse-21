package ru.nalimov.tm.repository;

import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepository extends AbstractRepository<Project> {

    private static ProjectRepository instance = null;

 //   private final List<Project> projects = new ArrayList<>();

 // public List<Project> findAll() {        return entityList;    }

 //   private final HashMap<String, List<Project>> projectsName = new HashMap<>();

//    public ProjectRepository() {
//    }

    public static ProjectRepository getInstance() {
        synchronized (ProjectRepository.class) {
            return instance == null
                    ? instance = new ProjectRepository()
                    : instance;
        }
    }

    public Project create(final String name, String description, Long userid) {
        final Project project = new Project(name);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userid);
//        projects.add(project);
        super.create(project);
        List<Project> projectsHashMap = entityMap.get(project.getName());
        if (projectsHashMap == null) projectsHashMap = new ArrayList<>();
        projectsHashMap.add(project);
        entityMap.put(project.getName(), projectsHashMap);
        return project;
    }

    public Project create(final String name, final UUID userId) {
        final Project project = new Project(name);
        if (userId != null)
//        projects.add(project);
            super.create(project);
        return project;
    }


    public int create(final String name) {
        final Project project = new Project(name);
//        projects.add(project);
        super.create(project);
        System.out.println("TEST");
        return 0;
    }

    public Project update(final Long id, final String name, final String description) throws ProjectNotFoundException {
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException("PROJECT NOT FOUND");
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
//        projects.add(project);
        super.create(project);
        return project;
    }

    public Project findByIndex(int index) throws ProjectNotFoundException {
        if (index < 0 || index > entityList.size() -1) throw new ProjectNotFoundException("NO INDEX");
        return entityList.get(index);
    }

    public List<Project> findByName(final String name) throws ProjectNotFoundException {
        final List<Project> projects = new ArrayList<>();
        if (entityMap.get(name) == null) throw new ProjectNotFoundException("PROJECT NOT FOUND");
        for (final Project project: entityMap.get(name)) {
//        projects.add(project);
            super.create(project);
        }
        return projects;
    }


    public Project findById(final Long id) throws ProjectNotFoundException {
        if (id == null)
            throw new ProjectNotFoundException("PROJECT ID IS EMPTY");
        for (final Project project: entityList) {
            if (project.getId().equals(id))
                return project;
        }
        throw new ProjectNotFoundException("PROJECT NOT FOUND BY ID");
    }


    public Project removeById(final Long id) throws ProjectNotFoundException {
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException("NO ID");
//        projects.remove(project);
//        projectsName.remove(project.getName());
        entityList.remove(project);
        removeFromEntityMap(project);
        return project;
    }

    public Project removeByIndex (final int index) throws ProjectNotFoundException {
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException("NO INDEX");
//        projects.remove(project);
//        projectsName.remove(index);
        entityList.remove(project);
        removeFromEntityMap(project);

        return project;
    }

    public void clear() {
//        projects.clear();
//        projectsName.clear();
        entityList.clear();
        entityMap.clear();
    }

    public void clearProjectsByUserId(final Long userId) throws ProjectNotFoundException {
        final List<Project> projects = findAllByUserId(userId);
        if (projects.isEmpty())
            throw new ProjectNotFoundException("NO PROJECT FOR USER");
        for (Project project : projects){
            removeById(project.getId());
        }
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> projectsWithUser = new ArrayList<>();
        for (final Project project : findAll()) {
            if (project.getUserId() != null && project.getUserId().equals(userId))
                projectsWithUser.add(project);
        }
        return projectsWithUser;
    }

    public int size() { return entityList.size(); }

    public List<Project> findAll() { return entityList; }

}

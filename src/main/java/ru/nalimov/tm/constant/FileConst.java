package ru.nalimov.tm.constant;

public class FileConst {
    public class FileNameConst {
        public static final String PROJECT_JSON ="project_json.out";
        public static final String TASK_JSON ="task_json.out";
        public static final String USER_JSON ="user_json.out";

        public static final String PROJECT_XML ="project.xml";
        public static final String TASK_XML ="task.xml";
        public static final String USER_XML ="user.xml";
    }
}

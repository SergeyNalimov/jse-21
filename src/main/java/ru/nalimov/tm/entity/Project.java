package ru.nalimov.tm.entity;

import java.util.Comparator;

public class Project {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long userid = System.nanoTime();

    public Project(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserId() {
        return userid;
    }

    public void setUserId(final Long userId) {
        this.userid = userId;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userid=" + userid +
                '}';
    }

    public static Comparator<Project> ProjectSortByName = new Comparator<Project>() {
    @Override
        public int compare(Project p1, Project p2) {
            return p1.getName().compareTo(p2.getName());
        }
    };

}

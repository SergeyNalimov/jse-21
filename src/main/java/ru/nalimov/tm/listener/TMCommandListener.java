package ru.nalimov.tm.listener;

import ru.nalimov.tm.enumerated.Command;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.service.*;

import java.io.IOException;

public class TMCommandListener implements CommandListener {

    private static final ProjectService projectService = ProjectService.getInstance();

    private static final TaskService taskService = TaskService.getInstance();

    private static final SystemService systemService = SystemService.getInstance();

    private static final UserService userService = UserService.getInstance();

    private static final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    public static Long id = null;

    @Override
    public void execute(Command command) throws ProjectNotFoundException, TaskNotFoundException, IOException {
        switch (command) {
            case PROJECT_CREATE: projectService.createProject();
                break;
            case PROJECT_CLEAR: projectService.clear();
                break;
            case PROJECT_LIST: projectService.listProject();
                break;
            case PROJECT_VIEW_BY_ID: projectService.viewProjectById();
                break;
            case PROJECT_VIEW_BY_INDEX: projectService.viewProjectByIndex();
                break;
            case PROJECT_REMOVE_BY_ID: projectService.removeProjectById();
                break;
            case PROJECT_REMOVE_BY_INDEX: projectService.removeProjectByIndex();
                break;
            case PROJECT_UPDATE_BY_INDEX: projectService.updateProjectByIndex();
                break;
            case PROJECT_UPDATE_BY_ID: projectService.updateProjectById();
                break;

            case TASK_CREATE: taskService.createTask();
                break;
            case TASK_CLEAR: taskService.clearTask();
                break;
            case TASK_LIST: taskService.listTask();
                break;
            case TASK_VIEW_BY_INDEX: taskService.viewTaskByIndex();
                break;
            case TASK_VIEW_BY_ID: taskService.viewTaskById();
                break;
            case TASK_REMOVE_BY_ID: taskService.removeTaskById();
                break;
            case TASK_REMOVE_BY_INDEX: taskService.removeTaskByIndex();
                break;
            case TASK_UPDATE_BY_INDEX: taskService.updateTaskByIndex();
                break;
            case TASK_UPDATE_BY_ID: taskService.updateTaskById();
                break;
            case TASK_ADD_PROJECT_BY_IDS: taskService.addTaskToProjectByIds();
                break;
            case TASK_REMOVE_PROJECT_BY_IDS: taskService.removeTaskProjectByIds();
                break;
            case TASK_LIST_BY_PROJECT_ID: taskService.listTaskByProjectId();
                break;
            case TASK_ADD_TO_USER_BY_IDS: taskService.addTaskToUser();
                break;
            case TASK_CLEAR_BY_USER_ID: taskService.clearTasksByUserId();
                break;
            case TASK_LIST_BY_USER_ID: taskService.viewTaskListByUserId();
                break;

            case USER_CREATE: userService.createUser();
                break;
            case ADMIN_CREATE: userService.createUser(Role.ADMIN);
                break;
            case USERS_CLEAR: userService.clearUsers();
                break;
            case USERS_LIST: userService.listUsers();
                break;
            case USER_VIEW_BY_LOGIN: userService.viewUserByLogin();
                break;
            case USER_REMOVE_BY_LOGIN: userService.removeUserByLogin();
                break;
            case USER_UPDATE_BY_LOGIN: userService.updateUserByLogin();
                break;
            case USER_VIEW_BY_ID: userService.viewUserById();
                break;
            case USER_REMOVE_BY_ID: userService.removeUserById();
                break;
            case USER_UPDATE_BY_ID: userService.updateUserById();
                break;
            case USER_AUTHENTIC: userService.userAuthentic();
                break;
            case USER_DEAUTHENTIC: userService.userDeauthentic();
                break;
            case USER_CHANGE_PASSWORD: userService.userChangePassword();
                break;
            case USER_UPDATE_ROLE: userService.updateRole();
                break;
            case USER_PROFILE_UPDATE: userService.updateProfile(id);
                break;
            case USER_PROFILE_VIEW: userService.userProfile(id);
                break;
            case PROJECT_ADD_TO_USER_BY_IDS: projectService.addProjectToUser();
                break;
            case PROJECT_CLEAR_BY_USER_ID: projectService.clearProjectsByUserId();
                break;
            case PROJECT_LIST_BY_USER_ID: projectService.viewProjectListByUserId();
                break;
            case PROJECT_WRITE_JSON: projectService.writeProjectJson();
                break;
            case PROJECT_WRITE_XML: projectService.writeProjectXML();
                break;
            case PROJECT_READ_JSON: projectService.readProjectJson();
                break;
            case PROJECT_READ_XML: projectService.readProjectXML();
                break;
            case TASK_WRITE_JSON: taskService.writeTaskJson();
                break;
            case TASK_WRITE_XML: taskService.writeTaskXML();
                break;
            case TASK_READ_JSON: taskService.readTaskJson();
                break;
            case TASK_READ_XML: taskService.readTaskXML();
                break;
            default: systemService.displayErr();
        }
    }
}

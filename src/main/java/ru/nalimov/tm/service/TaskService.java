package ru.nalimov.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nalimov.tm.Application;
import ru.nalimov.tm.entity.Task;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.repository.TaskRepository;

import java.io.*;
import java.util.Collections;
import java.util.List;

import static ru.nalimov.tm.constant.FileConst.FileNameConst.*;

public class TaskService extends AbstractController  {

    private final Logger logger = LogManager.getLogger(TaskService.class);
    private static final TaskRepository taskRepository = TaskRepository.getInstance();
    private static TaskService instance = null;
    private final ProjectTaskService projectTaskService;
    private final UserService userService;
    private final ProjectService projectService;

    //   private ProjectTaskService projectTaskService;
    //private Scanner scanner;

    public TaskService() {
        this.projectTaskService = ProjectTaskService.getInstance();
        this.userService = UserService.getInstance();
        this.projectService = ProjectService.getInstance();
    }

    public static TaskService getInstance() {
        synchronized (TaskService.class) {
            return instance == null
                    ? instance = new TaskService()
                    : instance;
        }
    }

    public int createTask() {
        System.out.println("[CREATE_TASK]");
        logger.info("[CREATE_TASK]");
        System.out.println("INPUT TASK NAME");
        final String name = scanner.nextLine();
        System.out.println("PLEASE ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        final Long userId = Application.userIdCurrent;
        taskRepository.create(name, description, userId);
        System.out.println("OK");
        return 0;
    }

    public Task create(String name, String description, Long userid) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        logger.trace("CREATE TASK " + name + " " + description + " " + userid);
        return taskRepository.create(name, description, userid);
    }
//
//    public Task update(Long id, String name, String description) throws TaskNotFoundException {
//        if (id == null) return null;
//        if (name == null || name.isEmpty()) return null;
//        if (description == null || description.isEmpty()) return null;
//        logger.trace("UPDATE TASK " + name + " " + description + " " + id);
//        return taskRepository.update(id, name, description);
//    }
//
//    public Task findByIndex(int index) throws TaskNotFoundException {
//        if (index < 0) return null;
//        return taskRepository.findByIndex(index);
//    }
//
//    public Task removeById(Long id) throws TaskNotFoundException {
//        if (id == null) return null;
//        logger.trace("REMOVE TASK BY ID " + id);
//        return taskRepository.removeById(id);
//    }
//
//    public Task removeByIndex(int index) throws TaskNotFoundException {
//        if (index < 0) return null;
//        return taskRepository.removeByIndex(index);
//    }
//
//    public Task findById(Long id) throws TaskNotFoundException {
//        if (id == null) return null;
//        return taskRepository.findById(id);
//    }
//
//    public void clear() {
//        taskRepository.clear();
//    }
//
//    public List<Task> findAll() {
//        return taskRepository.findAll();
//    }
//
//    public List<Task> findAllByProjectId(Long projectId) {
//        if (projectId == null) return null;
//        return taskRepository.findAddByProjectId(projectId);
//    }
public int clearTask() {
    System.out.println("[CLEAR_TASK]");
    logger.info("[CLEAR_TASK]");
    taskRepository.clear();
    System.out.println("[OK]");
    return 0;
}

    public int viewTask(final Task task) {
        System.out.println("[VIEW TASK]");
        System.out.println("ID: "+task.getId());
        System.out.println("NAME: "+task.getName());
        System.out.println("DESCRIPTION: "+task.getDescription());
        System.out.println("[OK]");
        return 0;
    }

    public int viewTaskByIndex() throws TaskNotFoundException {
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task;
        try {
            task = taskRepository.findByIndex(index);
            viewTask(task);
            return 0;
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }


    public int viewTaskById() throws TaskNotFoundException {
        System.out.println("ENTER, TASK INDEX:");
        final long id = scanner.nextLong();
        final Task task = taskRepository.findById(id);
        viewTask(task);
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        logger.info("UPDATE TASK BY INDEX");
        System.out.println("PLEASE, ENTER TASK INDEX");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task;
        try {
            task = taskRepository.findByIndex(vId);
            System.out.println("PLEASE, ENTER TASK NAME");
            final String name = scanner.nextLine();
            System.out.println("PLEASE, ENTER TASK DESCRIPTION");
            final String description = scanner.nextLine();
            if (taskRepository.update(task.getId(), name, description) != null) {
                System.out.println("[OK]");
            } else {
                System.out.println("[FAIL]");
            }
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
        return 0;
    }


    public int updateTaskById() throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        logger.info("UPDATE TASK BY ID");
        System.out.println("PLEASE, ENTER TASK ID");
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = taskRepository.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION");
        final String description = scanner.nextLine();
        taskRepository.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        logger.info("REMOVE TASK BY ID");
        System.out.println("PLEASE, ENTER TASK ID");
        final long id = scanner.nextLong();
        final Task task = taskRepository.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        logger.info("REMOVE TASK BY INDEX");
        System.out.println("PLEASE, ENTER TASK INDEX");
        final int index = scanner.nextInt() - 1;
        final Task task = taskRepository.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }


    public int listTask() {
        System.out.println("[LIST_TASK]");
        int index = 1;
        viewTasks(taskRepository.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        TaskSortByName(tasks);
        for (final Task task: taskRepository.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASK TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectByIds() throws TaskNotFoundException, ProjectNotFoundException {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        logger.info("ADD TASK TO PROJECT BY IDS");
        System.out.println("PLEASE, ENTER PROJECT ID");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[ADD TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskProjectByIds() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        logger.info("REMOVE TASK TO PROJECT BY IDS");
        System.out.println("PLEASE, ENTER PROJECT ID");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[REMOVE TASK FROM ID]");
        System.out.println("PLEASE, ENTER TASK ID");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }


    public int addTaskToUser() {
        System.out.println("ADDING TASK TO USER");
        logger.info("ADDING TASK TO USER");
        try {
            System.out.print("Enter task id: ");
            final Long taskId = Long.parseLong(scanner.nextLine());
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            final Task task = addTaskToUser(taskId, userId);
            System.out.println("Added user to task: " + task.toString());
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                addTaskToUser();
        }
        return 0;
    }

    public int clearTasksByUserId() {
        System.out.println("CLEARING TASKS BY USERID");
        logger.info("CLEARING TASKS BY USERID");
        try {
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            taskRepository.clearTasksByUserId(userId);
            System.out.println("Removed all tasks with user id " + userId + " ");
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                clearTasksByUserId();
        }
        return 0;
    }

    public int viewTaskListByUserId() {
        System.out.println(" VIEWING TASKS BY USERID ");
        logger.info(" VIEWING TASKS BY USERID ");
        try {
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            viewTasks(taskRepository.findAllByUserId(userId));
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                viewTaskListByUserId();
        }
        return 0;
    }

    public int viewTaskListByUserId(final Long userId) {
        System.out.println("VIEWING TASKS BY USERID " + userId + " ");
        viewTasks(taskRepository.findAllByUserId(userId));
        return 0;
    }


    public Task addTaskToUser(Long taskId, Long userId) throws TaskNotFoundException {
        if (taskId == null)
            return null;
        if (userId == null)
            return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null)
            return null;
        task.setUserId(userId);
        return task;
    }

    public void clearTasksByUserId(Long userId) throws TaskNotFoundException {
        if (userId == null)
            return;
        taskRepository.clearTasksByUserId(userId);
    }

    public List<Task> findAllByUserId(Long userId) {
        if (userId == null)
            return null;
        return taskRepository.findAllByUserId(userId);
    }

    public List<Task> TaskSortByName(List<Task> tasks) {
        Collections.sort(tasks, Task.TaskSortByName);
        return tasks;
    }

    public int writeTaskJson() {
        final List<Task> tasks = findAll();
        if (tasks == null || tasks.isEmpty()) System.out.println("[FAIL]");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String prettyJson = "";
        try {
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(TASK_JSON));
            prettyJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
            objectOutputStream.writeObject(prettyJson);
            objectOutputStream.close();
        } catch (JsonProcessingException | FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("TaskS WRITTEN TO FILE");
        return 0;
    }
    public int writeTaskXML() {
        final List<Task> tasks = findAll();
        if (tasks == null || tasks.isEmpty()) System.out.println("[FAIL]");
        XmlMapper xmlMapper = new XmlMapper();
        File file = new File(TASK_XML);
        try {
            xmlMapper.writeValue(file, tasks);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("TASKS WRITTEN TO FILE");
        return 0;
    }
    public List<Task> findAll() {
        return taskRepository.findAll();
    }


    public int readTaskJson() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {
            Task taskFromJackson= objectMapper.readValue(TASK_JSON, Task.class);
            System.out.println("taskFromJackson = " + taskFromJackson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int readTaskXML() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        File file = new File("task.xml");
        try {
            String xml = inputStreamToString(new FileInputStream(file));
            //  System.out.println("1 "+Task.class);
            Task newTask = xmlMapper.readValue(xml, Task.class);
            System.out.println("taskFromXML = " + newTask);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public static String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
